<?php

require '../vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
 * Start the implementation for a PHP >= 7.4 framework which strictly meets (accepted) PSR standards.
 * https://www.php-fig.org/psr/
 *
 * Your part of this project is to implement the Routing
 * which leads into an instantiation and invocation of an HTTP-Handler (PSR-15).
 *
 * Following the open close principle realize a routing solution
 * that allows to plug in easily new routing strategies when needed.
 *
 * We do not want to implement the HTTP Message Interface (PSR-7) on our own.
 * Therefore a package has been already installed to address this part.
 *
 * BONUS:
 * Use DI to provide the HTTP Handler with all its dependencies.
 * Use a configurable definition of the handler's dependencies within a Container (PSR-11).
 *
 * /


/*
 * You can start using the installed packages and those examples:
 */

// HTTP factory to create ServerRequest and ServerResponse
$psr17Factory = new \Nyholm\Psr7\Factory\Psr17Factory();

// create ServerRequest
$creator = new \Nyholm\Psr7Server\ServerRequestCreator(
    $psr17Factory, // ServerRequestFactory
    $psr17Factory, // UriFactory
    $psr17Factory, // UploadedFileFactory
    $psr17Factory  // StreamFactory
);
$serverRequest = $creator->fromGlobals();

// create ServerResponse
$responseBody = $psr17Factory->createStream('Hello g-portal!');
$response = $psr17Factory->createResponse(200)->withBody($responseBody);

// emit ServerResponse
(new \Laminas\HttpHandlerRunner\Emitter\SapiEmitter())->emit($response);

