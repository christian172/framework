## Installation

install composer
https://getcomposer.org/download/

run 
`composer i`
inside the project directory

## Task

A new framework has to be created for PHP >= 7.4 which
strictly meets (accepted) PSR standards.
https://www.php-fig.org/psr/

Your part of this project is to implement the Routing
which leads into an instantiation and invocation of an HTTP-Handler (PSR-15).

Following the open close principle realize a routing solution
that allows to plug in easily further routing strategies later when needed.

We do not want to implement the HTTP Message Interface (PSR-7) on our own.
Therefore a package has been already installed to address this part.

## Bonus

Use DI to provide the HTTP Handler with all its dependencies.
Configure the definition of the handler's dependencies within a Container (PSR-11).

## Help

You can start using the installed packages and those examples:

https://gitlab.com/christian172/framework
